from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoListForm, TodoItemForm, TodoItem


def show_todo_lists(request):
    todolists = TodoList.objects.all()
    return render(request, "list.html", {"todolist_list": todolists})


def show_todo_list(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    return render(request, "detail.html", {"todolist": todolist})


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        form = TodoListForm()
    return render(request, "create.html", {"form": form})


def update_todo_list(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        form = TodoListForm(request. POST, instance=todolist)
        if form. is_valid():
            todolist = form. save()
        return redirect("todo_list_detail", todolist.pk)
    else:
        todolist = get_object_or_404(TodoList, pk=pk)
        form = TodoListForm(instance=todolist)
    return render(request, "update.html", {"form": form})


def delete_todo_list(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "itemcreate.html", {"form": form})


def update_todo_item(request, id):
    todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    return render(request, "itemupdate.html", {"form": form})
